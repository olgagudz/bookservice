import { Input } from '@angular/core';
import { Component} from '@angular/core';
import { Book } from 'src/app/models/book.model';

@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.css']
})
export class BookListItemComponent  {
 @Input() book!: Book;
 @Input() index!: number;
}
