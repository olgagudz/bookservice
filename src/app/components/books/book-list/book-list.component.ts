import { Component, OnDestroy, OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
import { Book } from 'src/app/models/book.model';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
})
export class BookListComponent implements OnInit, OnDestroy {
  private booksSub!: Subscription;

  books: Book[] = [];

  constructor(private booksService: BooksService) {}

  ngOnInit(): void {
    this.booksSub = this.booksService.booksLoaded.subscribe(
      (books: Book[]) => {
        this.books = books;
      }
    );
  }

  ngOnDestroy(){
    this.booksSub.unsubscribe();
  }

}
