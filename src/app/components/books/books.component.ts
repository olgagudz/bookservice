import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  author = "";

  constructor(private booksService:BooksService) { }

  ngOnInit(): void { }

  onSearchByAuthor(){
    this.booksService.getByAuthor(this.author);
  }

  onGetAllBooks(){
    this.author = "";
    this.booksService.getAllBooks();
  }


}
