import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Reader } from 'src/app/models/reader.model';
import { ReadersService } from 'src/app/services/readers.service';

@Component({
  selector: 'app-readers',
  templateUrl: './readers.component.html',
  styleUrls: ['./readers.component.css']
})
export class ReadersComponent implements OnInit, OnDestroy {
  private readerSub!: Subscription;

  readers: Reader[] = [];
  name: string = "";

  constructor(private readerService: ReadersService) { }

  ngOnInit(): void {
    this.readerSub = this.readerService.readersLoaded.subscribe(
      (readers : Reader[]) => {
        this.readers = readers;
      }
    );
  }

  onGetAllReaders(){
    this.name = "";
    this.readerService.getAllReaders();
  }

  onSearchReader(){
    this.readerService.getReaderByName(this.name);
  }

  ngOnDestroy(){
    this.readerSub.unsubscribe();
  }

}
