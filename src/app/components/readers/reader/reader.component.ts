import { Component, Input, OnInit } from '@angular/core';
import { Reader } from 'src/app/models/reader.model';

@Component({
  selector: 'app-reader',
  templateUrl: './reader.component.html',
  styleUrls: ['./reader.component.css']
})
export class ReaderComponent implements OnInit {
  @Input() reader!: Reader;

  constructor() { }

  ngOnInit(): void {
  }

}
