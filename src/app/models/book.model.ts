export class Book {
  public isbn : string;
  public author: string;
  public name:string;
  public available:number;
  public catalog : {
    bookshelf: string
  }

  constructor(isbn : string,author: string,name:string,available:number, bookshelf: string ){
    this.isbn = isbn;
    this.author = author;
    this.name = name;
    this.available = available;
    this.catalog = { bookshelf: bookshelf};
  }
}
