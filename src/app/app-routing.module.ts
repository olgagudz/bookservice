import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookDetailsComponent } from './components/books/book-details/book-details.component';
import { BookStartComponent } from './components/books/book-start/book-start.component';
import { BooksComponent } from './components/books/books.component';
import { ReadersComponent } from './components/readers/readers.component';

const routes: Routes = [
  {path: '', redirectTo: '/books', pathMatch: "full"},
  {path: 'books', component:BooksComponent, children: [
    {path: '', component: BookStartComponent},
    {path: ':id', component: BookDetailsComponent}
  ]},
  {path: 'readers', component:ReadersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
