import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { BooksComponent } from './components/books/books.component';
import { BookListComponent } from './components/books/book-list/book-list.component';
import { BookDetailsComponent } from './components/books/book-details/book-details.component';
import { ReadersComponent } from './components/readers/readers.component';
import { BookListItemComponent } from './components/books/book-list/book-list-item/book-list-item.component';
import { BooksService } from './services/books.service';
import { BookStartComponent } from './components/books/book-start/book-start.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReaderComponent } from './components/readers/reader/reader.component';
import { ReadersService } from './services/readers.service';
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BooksComponent,
    BookListComponent,
    BookDetailsComponent,
    ReadersComponent,
    BookListItemComponent,
    BookStartComponent,
    ReaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ScrollingModule
  ],
  providers: [BooksService, ReadersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
