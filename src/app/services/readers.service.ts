import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Reader } from "../models/reader.model";

@Injectable()
export class ReadersService {
  readersLoaded = new Subject< Reader[]>();
  private readers: Reader[] = [
    new Reader("1", "Olga", "Gudz"),
    new Reader("2", "Sergey", "Stol")
  ];

  constructor(private http: HttpClient){}

  getAllReaders(){
    //this.readers = [];
    // http request + subscribe
    this.readersLoaded.next(this.readers);
  }

  getReaderByName(name: string){

  }
}
