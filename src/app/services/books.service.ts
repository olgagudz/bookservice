import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Book } from '../models/book.model';

@Injectable()
export class BooksService {
  bookSelected = new Subject<Book>();
  booksLoaded = new Subject<Book[]>();
  private books: Book[] = [];

  constructor(private http: HttpClient, private router: Router){}

  getBook(index: number) {
    return this.books.slice()[index];
  }

  getAllBooks(){
    if(this.books.length !== 0){
      this.booksLoaded.next(this.books);
      this.router.navigateByUrl("/books");
      return;
    }
    this.http.get< {[key: string]: Book}>('https://bookservice-c372d-default-rtdb.firebaseio.com/books.json')
    .pipe(tap((responseData : {[key: string]: Book})  => {
         for(const key in responseData){
           this.books.push(
             new Book(responseData[key].isbn,
               responseData[key].author,
               responseData[key].name,
               +responseData[key].available,
               responseData[key].catalog.bookshelf)
           );
         }
         this.booksLoaded.next(this.books);
         this.router.navigateByUrl("/books");
        }
    ))
    .subscribe();
  }

  getByAuthor(author: string){
    //this.books = [];
    //this.booksLoaded.next(this.books);
    const booksByAuthor : Book[] = [];
    for(let book of this.books){
      if(book.author.toLowerCase().includes(author.toLowerCase())){
        booksByAuthor.push(book);
      }
    }
    this.booksLoaded.next(booksByAuthor);
    this.router.navigateByUrl("/books");
  }

}
